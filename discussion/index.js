//[SECTION]	Create an API Server using Express

//Identify the proper ingredient/material/components needed to start the project.

	//use the 'require' directive to load the express module or package.
	//expree => thas will allow us to access methods and function that will creating a server easier.
const express = require('express')

	//create an application using express.
	//express() -> this creates an express/instant applicatiopn and we will give an identifier for the app that it will produce.
	//In layman's term, the application will be our server.
const application = express();

	//identify a virtual port in which to serve the project.

const port = 4000;

	//assign th connection or the established connection/server into the designated port.

application.listen(port, () => console.log(`Server is running on ${port}`));